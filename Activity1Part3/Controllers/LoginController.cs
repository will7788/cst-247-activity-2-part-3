﻿using Activity1Part3.Models;
using Activity1Part3.Services.Business;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Activity1Part3.Controllers
{
    public class LoginController : Controller
    {
        private static Logger logger = LogManager.GetLogger("myAppLoggerRules");
        // GET: Login
        [HttpGet]
        public ActionResult Index()
        {
            return View("Login");
        }
        [HttpPost]

        public ActionResult Login(UserModel model)
        {
            try {
                logger.Info("Entering LoginController.DoLogin()");
                SecurityService service = new SecurityService();
                bool result = service.Authenticate(model);
                logger.Info("Parameters are:" + new JavaScriptSerializer().Serialize(model));
                if (!ModelState.IsValid)
                {
                    return View("Login");
                }
                if (result == true)
                {
                    logger.Info("Exit LoginController.DoLogin() with login passing");
                    return View("LoginPassed", model);
                }
                else
                {
                    logger.Info("Exit LoginController.DoLogin() with login failing");
                    return View("LoginFailed");
                }
            }
            catch (Exception e) 
            {
                logger.Error("Exception LoginController.DoLogin()"+ e.Message);
                return View("Login");
            }

        }
    }
}