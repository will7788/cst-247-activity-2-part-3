﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Activity1Part3.Services.Utility
{
    public class MyLogger1 : ILogger
    {
        private static Lazy<MyLogger1> obj;
        private MyLogger1() {
        }

        public static Lazy<MyLogger1> getInstance() 
        {
            if (obj == null)
            {
                obj = new Lazy<MyLogger1>();
            }
            return obj;
        }
        public static Lazy<Logger> GetLogger()
        {
            Lazy<Logger> log = new Lazy<Logger>();
            return log;
        }
        public void Debug(string message)
        {
            throw new NotImplementedException();
        }

        public void Error(string message)
        {
            throw new NotImplementedException();
        }

        public void Info(string message)
        {
            throw new NotImplementedException();
        }

        public void Warning(string message)
        {
            throw new NotImplementedException();
        }
    }
}